provider"aws" {
  region = "us-east-1"
}

variable "cidr_block" {
  description = "cidr for subnet and vpc"
}

resource "aws_vpc" "Devlopment-vpc" {
  cidr_block = var.cidr_block[1]
  tags ={
    Name = "Devlopment"
  }
}

resource "aws_subnet" "dev-sub-1" {
  cidr_block = var.cidr_block[0]
  vpc_id = aws_vpc.Devlopment-vpc.id
  availability_zone = "us-east-1a"
  tags = {
    Name = "Subnet-dev01"
  }
}

data "aws_vpc" "existing-vpc"  {
  cidr_block = "172.31.0.0/16"

}

resource "aws_subnet" "test-sub-1" {
  vpc_id = data.aws_vpc.existing-vpc.id
  cidr_block = "172.31.96.0/19"
  availability_zone = "us-east-1a"
  tags = {
    Name = "Subnet-for-test"
  }
}

